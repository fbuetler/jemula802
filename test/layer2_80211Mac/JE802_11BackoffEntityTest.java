package layer2_80211Mac;

import com.ginsberg.junit.exit.ExpectSystemExitWithStatus;
import emulator.JE802StatEval;
import gui.JE802Gui;
import kernel.JEEvent;
import kernel.JEEventScheduler;
import kernel.JETime;
import layer0_medium.JEWirelessMedium;
import layer1_80211Phy.JE802_11Phy;
import layer1_802Phy.JE802PhyMode;
import layer3_network.JE802IPPacket;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class JE802_11BackoffEntityTest {

    private JE802_11BackoffEntity be;
    private JE802_11Mac mac;
    private final static String macNode = "<JE80211MAC dot11MacAddress4_byte=\"6\" dot11MacFCS_byte=\"4\"\n" +
            "\t\t\tdot11MacHeaderACK_byte=\"10\" dot11MacHeaderCTS_byte=\"10\"\n" +
            "\t\t\tdot11MacHeaderDATA_byte=\"24\" dot11MacHeaderRTS_byte=\"16\"\n" +
            "\t\t\tdot11WepEncryption=\"false\">\n" +
            "\n" +
            "\t\t\t<JE802Mlme NameOfAlgorithm=\"phymode_6Mbps\"\n" +
            "\t\t\t\tComputingInterval_ms=\"100\" ShowPlot=\"false\">\n" +
            "\t\t\t\t<!-- // MLME = MAC Layer Management Entity -->\n" +
            "\t\t\t\t<!-- // \"NameOfAlgorithm\" determines the actual algorithm used for dynamically \n" +
            "\t\t\t\t\tadapting the MAC parameters. This string value will be compared to the list \n" +
            "\t\t\t\t\tof existing algorithms by the mobile computing entity. Use \"none\" for static \n" +
            "\t\t\t\t\tbehavior. -->\n" +
            "\t\t\t\t<!-- // \"ComputingInterval_ms\" will set the period, i.e., the time interval \n" +
            "\t\t\t\t\tbetween each computing event, given in milliseconds -->\n" +
            "\t\t\t\t<!-- // \"ShowPlot\" set to true will switch on a small plot showing the \n" +
            "\t\t\t\t\tparameters used. For example the value of the parameter AIFSN can be seen -->\n" +
            "\t\t\t</JE802Mlme>\n" +
            "\n" +
            "\n" +
            "\t\t\t<JE802BackoffEntity AC=\"1\" queuesize=\"10\">\n" +
            "\t\t\t\t<MIB802.11e dot11EDCAAIFSN=\"2\" dot11EDCACWmax=\"1023\"\n" +
            "\t\t\t\t\tdot11EDCACWmin=\"15\" dot11EDCAMSDULifetime=\"1000\" dot11EDCAPF=\"2.0\"\n" +
            "\t\t\t\t\tdot11EDCATXOPLimit=\"3008\" />\n" +
            "\t\t\t</JE802BackoffEntity>\n" +
            "\t\t\t<JE802BackoffEntity AC=\"2\" queuesize=\"10\">\n" +
            "\t\t\t\t<MIB802.11e dot11EDCAAIFSN=\"4\" dot11EDCACWmax=\"1023\"\n" +
            "\t\t\t\t\tdot11EDCACWmin=\"15\" dot11EDCAMSDULifetime=\"1000\" dot11EDCAPF=\"2.0\"\n" +
            "\t\t\t\t\tdot11EDCATXOPLimit=\"3008\" />\n" +
            "\t\t\t</JE802BackoffEntity>\n" +
            "\t\t\t<JE802BackoffEntity AC=\"3\" queuesize=\"10\">\n" +
            "\t\t\t\t<MIB802.11e dot11EDCAAIFSN=\"2\" dot11EDCACWmax=\"1023\"\n" +
            "\t\t\t\t\tdot11EDCACWmin=\"15\" dot11EDCAMSDULifetime=\"1000\" dot11EDCAPF=\"2.0\"\n" +
            "\t\t\t\t\tdot11EDCATXOPLimit=\"3008\" />\n" +
            "\t\t\t</JE802BackoffEntity>\n" +
            "\t\t\t<JE802BackoffEntity AC=\"4\" queuesize=\"10\">\n" +
            "\t\t\t\t<MIB802.11e dot11EDCAAIFSN=\"2\" dot11EDCACWmax=\"1023\"\n" +
            "\t\t\t\t\tdot11EDCACWmin=\"15\" dot11EDCAMSDULifetime=\"1000\" dot11EDCAPF=\"2.0\"\n" +
            "\t\t\t\t\tdot11EDCATXOPLimit=\"3008\" />\n" +
            "\t\t\t</JE802BackoffEntity>\n" +
            "\t\t\t<MIB802.11-1999 dot11BroadcastAddress=\"255\"\n" +
            "\t\t\t\tdot11FragmentationThreshold=\"512\" dot11LongRetryLimit=\"4\"\n" +
            "\t\t\t\tdot11MACAddress=\"2\" dot11MaxReceiveLifetime=\"1000\"\n" +
            "\t\t\t\tdot11MaxTransmitMSDULifetime=\"1000\" dot11RTSThreshold=\"500\"\n" +
            "\t\t\t\tdot11ShortRetryLimit=\"7\" />\n" +
            "\t\t</JE80211MAC>";

    @BeforeEach
    void setUp() throws Exception {
        makeMac();
        be = this.mac.getBackoffEntity(1);
    }

    private void makeMac() throws Exception {
        Node node =  DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(new ByteArrayInputStream(macNode.getBytes()))
                .getDocumentElement();

        JEEventScheduler scheduler = new JEEventScheduler();
        JE802StatEval statEval = mock(JE802StatEval.class);
        Random random  = mock(Random.class);
        JE802Gui gui = mock(JE802Gui.class);
        JEWirelessMedium channel = mock(JEWirelessMedium.class);
        JE802_11Phy phy = makePhy();

        mac = new JE802_11Mac(scheduler, statEval, random,gui, channel,node,10,phy);
    }

    private JE802_11Phy makePhy() {
        JE802_11Phy phy = mock(JE802_11Phy.class);
        when(phy.getSlotTime()).thenReturn(new JETime(10));
        when(phy.getSIFS()).thenReturn(new JETime(10));
        when(phy.getPLCPHeaderWithoutServiceField()).thenReturn(new JETime(10));
        when(phy.getPLCPPreamble()).thenReturn(new JETime(10));
        when(phy.getSymbolDuration()).thenReturn(new JETime(10));
        when(phy.getPLCPHeaderDuration()).thenReturn(new JETime(10));

        JE802PhyMode phyMode = mock(JE802PhyMode.class);
        when(phyMode.getBitsPerSymbol()).thenReturn(8);
        when(phy.getBasicPhyMode(any())).thenReturn(phyMode);
        when(phy.getCurrentPhyMode()).thenReturn(phyMode);

        return phy;
    }

    @Test
    void event_handler__update_backoff_timer_req(){
        JEEvent event = new JEEvent("update_backoff_timer_req", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }

    @Test
    @ExpectSystemExitWithStatus(0)
    void event_handler__contains_MLME(){
        JEEvent event = new JEEvent("MLME_undefined", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }

    @Test
    void event_handler__MSDUDeliv_req(){
        Vector<Object> parameters = new Vector<>();
        parameters.add(new JE802HopInfo(1,0));
        parameters.add(2);
        List<JE802HopInfo> hops = new ArrayList<JE802HopInfo>();
        hops.add(new JE802HopInfo(1,6));
        parameters.add(hops);
        parameters.add(mock(JE802IPPacket.class));
        parameters.add(0L);
        parameters.add(88);
        JEEvent event = new JEEvent("MSDUDeliv_req", be.getHandlerId(), new JETime(10),parameters);
        be.event_handler(event);
    }


    private JE802_11Mpdu makeMpdu() {
        JE802_11Mpdu  mpdu = new JE802_11Mpdu();
        mpdu.setAC(1);
        mpdu.setDA(2);
        JE802PhyMode phyMode =  mac.getPhy().getBasicPhyMode(mock(JE802PhyMode.class));
        mpdu.setPhyMode(phyMode);
        return mpdu;
    }

    @Test
    void event_handler__MPDUDeliv_req(){
        Vector<Object> parameters = new Vector<>();
        parameters.add(makeMpdu());
        JEEvent event = new JEEvent("MPDUDeliv_req", be.getHandlerId(), new JETime(10),parameters);
        be.event_handler(event);
    }

    @Test
    void event_handler__backoff_expired_ind(){
        JEEvent event = new JEEvent("backoff_expired_ind", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }

    @Test
    void event_handler__interframespace_expired_ind(){
        JEEvent event = new JEEvent("interframespace_expired_ind", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }

    @Test
    void event_handler__nav_expired_ind(){
        JEEvent event = new JEEvent("nav_expired_ind", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }

    @Test
    void event_handler__tx_timeout_ind(){
        JEEvent event = new JEEvent("tx_timeout_ind", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }

    @Test
    void event_handler__PHY_RxEnd_ind(){
        Vector<Object> parameters = new Vector<>();
        parameters.add(makeMpdu());
        JEEvent event = new JEEvent("PHY_RxEnd_ind", be.getHandlerId(), new JETime(10),parameters );
        be.event_handler(event);
    }

    @Test
    void event_handler__PHY_SyncStart_ind(){
        JEEvent event = new JEEvent("PHY_SyncStart_ind", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }

    @Test
    void event_handler__MPDUReceive_ind(){
        Vector<Object> parameters = new Vector<>();
        parameters.add(3);
        parameters.add(2);
        parameters.add(new JETime(14));
        JEEvent event = new JEEvent("MPDUReceive_ind", be.getHandlerId(), new JETime(10),parameters);
        be.event_handler(event);
    }

    @Test
    void event_handler__virtual_collision_ind(){
        JEEvent event = new JEEvent("virtual_collision_ind", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }

    @Test
    void event_handler__PHY_TxEnd_ind(){
        JEEvent event = new JEEvent("PHY_TxEnd_ind", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }

    @Test
    @ExpectSystemExitWithStatus(0)
    void event_handler__undefined_event(){
        JEEvent event = new JEEvent("Not_a_defined_event", be.getHandlerId(), new JETime(10));
        be.event_handler(event);
    }
    @Test
    void getAC() {
        assertEquals(1,(int)be.getAC());
    }

    @Test
    void getAIFS() {
        assertEquals(0,new JETime(30).compareTo(be.getAIFS()));
    }

    @Test
    void getDot11EDCAAIFSN() {
        assertEquals(2,be.getDot11EDCAAIFSN());
    }

    @Test
    void getDot11EDCACWmax() {
        assertEquals(1023,be.getDot11EDCACWmax());
    }

    @Test
    void getDot11EDCACWmin() {
        assertEquals(15,be.getDot11EDCACWmin());
    }

    @Test
    void getDot11EDCAMMSDULifeTime() {
        assertEquals(0,new JETime(1000.0).compareTo(be.getDot11EDCAMMSDULifeTime()));
    }

    @Test
    void getDot11EDCATXOPLimit() {
        assertEquals(3008,be.getDot11EDCATXOPLimit());
    }

    @Test
    void getDot11EDCAPF() {
        assertEquals(2.0,be.getDot11EDCAPF());
    }

    @Test
    void getTheCollisionCnt() {
        assertEquals(0,be.getTheCollisionCnt());
    }

    @Test
    void getQueueSize() {
        assertEquals(10,be.getQueueSize());
    }

    @Test
    void getCurrentQueueSize() {
        assertEquals(0,be.getCurrentQueueSize());
    }

    @Test
    void getMac() {
        assertSame(this.mac,be.getMac());
    }

    @Test
    void setMaxRetryCountShort() {
        be.setMaxRetryCountShort(100);
        assertEquals(100,be.getMaxRetryCountShort());
    }

    @Test
    void getMaxRetryCountShort() {
        assertEquals(0,be.getMaxRetryCountShort());
    }

    @Test
    void setMaxRetryCountLong() {
        be.setMaxRetryCountLong(100);
        assertEquals(100,be.getMaxRetryCountLong());
    }

    @Test
    void getMaxRetryCountLong() {
        assertEquals(0,be.getMaxRetryCountLong());
    }

    @Test
    void setAC() {
        be.setAC(100);
        assertEquals(100,(int)be.getAC());
    }

    @Test
    void setDot11EDCAAIFSN_less_than_one() {
        be.setDot11EDCAAIFSN(0);
        assertEquals(1, be.getDot11EDCAAIFSN());
    }

    @Test
    void setDot11EDCAAIFSN_over_one() {
        be.setDot11EDCAAIFSN(5);
        assertEquals(5, be.getDot11EDCAAIFSN());
    }

    @Test
    void setDot11EDCACWmax() {
        be.setDot11EDCACWmax(500);
        assertEquals(500, be.getDot11EDCACWmax());
    }

    @Test
    void setDot11EDCACWmin_less_than_one() {
        be.setDot11EDCACWmin(0);
        assertEquals(1, be.getDot11EDCACWmin());
    }

    @Test
    void setDot11EDCACWmin_over_one() {
        be.setDot11EDCACWmin(5);
        assertEquals(5, be.getDot11EDCACWmin());
    }

    @Test
    void setDot11EDCAPF() {
        be.setDot11EDCAPF(3.14);
        assertEquals(3.14,be.getDot11EDCAPF());
    }

    @Test
    void setDot11EDCATXOPLimit() {
        be.setDot11EDCATXOPLimit(123);
        assertEquals(123,be.getDot11EDCATXOPLimit());
    }

    @Test
    void setDot11EDCAMMSDULifeTime() {
        JETime time = new JETime(123.123);
        be.setDot11EDCAMMSDULifeTime(time);
        assertSame(time,be.getDot11EDCAMMSDULifeTime());
        assertEquals(0,new JETime(123.123).compareTo(be.getDot11EDCAMMSDULifeTime()));
    }

    @Test
    void discardQueue() {
        be.discardQueue();
    }

    @Test
    @Disabled
    void setDiscardedCounter() {
        // Not currently used anywhere
    }

    @Test
    @Disabled
    void getDiscardedCounter() {
        // Not currently used anywhere
    }

    @Test
    void getTime() {
        JETime time = new JETime(0);
        assertEquals(0,time.compareTo(be.getTime()));
    }

    @Test
    void getTheUniqueEventScheduler() {
    }

    @Test
    @Disabled
    void getLongRetryCount() {
        // Not currently used anywhere
    }

    @Test
    @Disabled
    void getShortRetryCount() {
        // Not currently used anywhere
    }

    @Test
    @Disabled
    void getOverallTPCtr() {
        // Not currently used anywhere
    }

    @Test
    @Disabled
    void getSpecificTPCtr() {
        // Not currently used anywhere
    }

    @Test
    @Disabled
    void getFaultToleranceThreshold() {
        // Not currently used anywhere
    }
}